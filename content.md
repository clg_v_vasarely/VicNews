---
style: img{border-radius:10px}
theme : colors
---

# Bienvenue sur le site internet de <aside>VicNews</aside>

<div style="float:left; margin-right:2em; " markdown> ![](https://lmdbt.fr/VicNews/LogoJournal.svg =100x100)</div> 
 
Vous retrouverez sur ce site-web les différentes éditions du journal des élèves du collège Victor Vasarely.

## VicNews - Edition n°4

<div style="text-align:center;">
<img src="https://digipage.app/uploads/5765c370-8448-45fc-9a72-139a75bb59da.jpg" width=40%;>
</div>


- Comment s'est passé Lancieux cette année ?
- D'où vient Halloween ?
- Conseils de lectures et films pour se faire (un peu) peur 
- La BD de la rédaction
- Jeux


### Comment s'est passé Lancieux ?
Les élèves de sixième ont eu la chance de participer à leur séjour d’intégration à Lancieux du mercredi 25 septembre 2024 au vendredi 27 septembre 2024. Un moment fort en découvertes et en activités sportives.

<div style="display: flex;">
 <img src="https://digipage.app/uploads/406ee894-2d47-4b4a-8375-13d69aa0e60b.jpg" width=35%!important; height=auto!important;><p>&nbsp;</p>
    <img src="https://digipage.app/uploads/b5b07e9a-3a88-47b5-bf3c-61ac9a1663cf.jpg" width=65%!important; height=auto!important;>
</div>

#### Les interviews

##### Interview de Mme Djarari

**Question :** *Depuis combien de temps participez-vous au séjour qui a lieu à Lancieux ?*  
**Mme Djarari :** Je participe au séjour de Lancieux depuis mon arrivée au collège de Collinée, c'est-à-dire depuis onze ans. Chaque année, je passe une journée avec les élèves de sixième dans le cadre du séjour d'intégration. Avant cela, je participais au séjour organisé à Pléneuf-Val-André où j'allais déjà passer une journée.

**Question :** *Combien de temps êtes-vous restée cette année ?*  
**Mme Djarari :** Je suis restée toute la journée du vendredi.

**Question :** *Comment ça s'est passé ?*  
**Mme Djarari :** Ça s'est très bien passé. Cela m'a permis de découvrir les élèves. Le matin, j'ai accompagné le groupe qui faisait du char à voile, et l'après-midi, j'avais un groupe sous ma responsabilité dans le cadre de la pêche à pied. Nous avons passé un moment agréable. Par contre, le temps n'était pas vraiment favorable, avec beaucoup de pluie et de vent.

**Question :** *D'accord, merci à vous. Bonne fin de journée.*  
**Mme Djarari :** Merci, à vous aussi.

![](https://digipage.app/uploads/b5b07e9a-3a88-47b5-bf3c-61ac9a1663cf.jpg)
![](https://digipage.app/uploads/406ee894-2d47-4b4a-8375-13d69aa0e60b.jpg)

##### Interview de Maëlys M.

**Question :** *Tu t'appelles comment ?*  
**Maëlys :** Je m’appelle Maëlys.

**Question :** *Est-ce que le séjour s'est bien passé ?*  
**Maëlys :** Moyen, moyen.

**Question :** *Qu'est-ce qui t'a plu ?*  
**Maëlys :** Le char à voile.

**Question :** *Pourquoi ?*  
**Maëlys :** Je sais pas.

**Question :** *Ton activité favorite ?*  
**Maëlys :** Le char à voile.

**Question :** *L'activité que tu as le moins aimée ?*  
**Maëlys :** Le catamaran.

**Question :** *Ok, ben, merci et au revoir.*  
**Maëlys :** Au revoir.


##### Interview d'Ayden A.

**Question :** *Comment tu t'appelles ?*  
**Ayden :** Je m'appelle Ayden.

**Question :** *Comment s'est passé ton séjour ?*  
**Ayden :** Mon séjour s'est très bien passé.

**Question :** *Qu'est-ce qui t'a plu ?*  
**Ayden :** Le catamaran, le kayak, et être avec des amis. C'était bien.

**Question :** *Pourquoi ça t'a plu ?*  
**Ayden :** On a failli tomber. C'était drôle.

**Question :** *Et c'est quoi ton activité préférée ?*  
**Ayden :** Le catamaran, je dirais, c'est plus drôle.

**Question :** *Quelle activité as-tu le moins aimée ?*  
**Ayden :** La pêche à pied, on ne pouvait pas aller où on voulait, on s'ennuyait.

**Question :** *Ok, merci et au revoir.*  
**Ayden :** Au revoir.

#### Un peu d'histoire...

La fête d'Halloween, célébrée chaque 31 octobre, est une source d'inspiration pour de nombreux films, livres et séries. Cette journée permet aux enfants de se déguiser en personnages effrayants et de participer à la traditionnelle "chasse aux bonbons", ou "trick or treat" en Amérique. Les adultes, quant à eux, s'occupent de sculpter des citrouilles et de décorer leurs maisons avec des éléments macabres tels que des toiles d'araignée et des chats noirs. 

Malgré les critiques sur son aspect commercial, Halloween a des origines qui remontent à plus de 2 000 ans dans la culture celtique. Pour en comprendre l'origine, il faut se tourner vers les Celtes, environ 600 ans avant Jésus-Christ. Ces derniers marquaient le début de leur nouvelle année au début de novembre, comme l'explique Nadine Cretin, historienne spécialisée. La fête de Samain, qui signifie "réunion", se déroulait lors du sixième jour de la lune de novembre, coïncidant avec la nuit du 31 octobre au 1er novembre. Cette date marquait l'ouverture de la saison froide, période durant laquelle les bétails étaient rentrés et les récoltes stockées. À cette époque, Halloween symbolisait "le retour de l'obscurité et la présence des êtres démoniaques". L'historienne souligne que cette célébration inspirait la peur, permettant au monde surnaturel d'interagir avec celui des vivants. Les enfants chantaient dans les rues, et leur venue était considérée comme magique ; ne pas leur offrir quelque chose pouvait entraîner une année de malchance. Ce rituel a évolué pour donner naissance au "trick or treat" dans les années 1930.

Sources : Nicole Cretin historienne, Géo.fr


#### Conseils de lectures et de films pour se faire (un peu) peur 

A lire :
- L’œuvre complète de Stephen King, roman
- Contes de vampires et autre revenant
- Encyclopédie des horreurs, documentaire
- Dracula de Bram Stocker en roman et en manga, un classique

A voir :
- Dracula de Francis Ford Coppola, interdit au moins de 12 ans,
- Sleepy Hollow de Tim Burton, interdit au moins de 12 ans,
- Carrie au bal du diable de Brian De Palma, interdit au moins de 12 ans,
- Les Noces Funèbres de Tim Burton
- Mercredi, série, saison 1
- Les valeurs de la famille Adams de Barry Sonnenfeld
- Beetlejuice 1 et 2 de Tim Burton
- Le projet Blair Witch de Daniel Myrick et Eduardo Sánchez, interdit au moins de 12 ans
- Hérédité de Ari Aster, interdit au moins de 12 ans
- It de Andy Muschietti, interdit au moins de 12 ans. 


### BD

### Jeux

![](https://digipage.app/uploads/cbaf9994-63c5-4853-9df8-066533955f00.png)